package com.goodsoftw.pannelloorariotreni.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import com.goodsoftw.pannelloorariotreni.ui.home.HomeViewModel;
import com.goodsoftw.pannelloorariotreni.ui.map.MapViewModel;
import com.goodsoftw.pannelloorariotreni.viewmodel.AppOrarioTreniViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel.class)
    abstract ViewModel bindHomeViewModel(HomeViewModel homeViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel.class)
    abstract ViewModel bindMapViewModel(MapViewModel mapViewModel);


    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(AppOrarioTreniViewModelFactory factory);
}
