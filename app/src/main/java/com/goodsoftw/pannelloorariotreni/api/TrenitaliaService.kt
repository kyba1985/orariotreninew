package com.goodsoftw.pannelloorariotreni.api

import io.reactivex.Flowable

import retrofit2.http.GET
import retrofit2.http.Path

interface TrenitaliaService {

    @GET("/viaggiatrenonew/resteasy/viaggiatreno/cercaNumeroTrenoTrenoAutocomplete/{train}")
    fun getTrainInfo(@Path("train") train: String): Flowable<String>
}
