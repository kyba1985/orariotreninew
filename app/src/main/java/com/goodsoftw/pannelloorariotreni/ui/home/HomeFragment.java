package com.goodsoftw.pannelloorariotreni.ui.home;

import android.arch.lifecycle.ViewModelProvider;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.goodsoftw.pannelloorariotreni.R;
import com.goodsoftw.pannelloorariotreni.di.Injectable;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public class HomeFragment extends Fragment implements Injectable{

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private Unbinder unbinder;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.home_fragment, container, false);
        unbinder = ButterKnife.bind(this,rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }



    @Override public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
