package com.goodsoftw.pannelloorariotreni

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import com.goodsoftw.pannelloorariotreni.ui.home.HomeFragment
import com.goodsoftw.pannelloorariotreni.ui.map.MapFragment
import jp.wasabeef.util.ext.replaceFragment
import jp.wasabeef.util.ext.setFragment
import kotlinx.android.synthetic.main.activity_main_bottom_navigation.*

class MainActivityBottomNavigation : BaseActivity() {

    override fun getLayoutResource(): Int {
        return R.layout.activity_main_bottom_navigation
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                replaceFragment(R.id.frame_main_content, ::HomeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_dashboard -> {
                replaceFragment(R.id.frame_main_content, ::MapFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notifications -> {
                replaceFragment(R.id.frame_main_content, ::HomeFragment)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        if (savedInstanceState == null) setFragment(R.id.frame_main_content, ::HomeFragment)
    }
}
