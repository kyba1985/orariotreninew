package com.goodsoftw.pannelloorariotreni;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


/**
 * Created by francescogodino on 10/01/17.
 */
public abstract class BaseActivity extends AppCompatActivity {

    protected Toolbar toolbar;

    protected abstract int getLayoutResource();

    public void setCustomTitle(int res_value) {
        setCustomTitle(getResources().getString(res_value));
    }

    public void setCustomTitle(String value) {
        if (toolbar != null) {
            toolbar.setTitle(value);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResource());

//        toolbar = (Toolbar) findViewById(R.id.toolbar);
//        if (toolbar != null) {
//            setCustomTitle(R.string.app_name_title);
//            setSupportActionBar(toolbar);
//        }

    }

    protected void setColorSwipeRefresh(SwipeRefreshLayout swipe) {
        swipe.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorPrimaryDark);
    }
}
